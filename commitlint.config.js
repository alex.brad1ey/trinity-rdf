const Configuration = {
  extends: ['@commitlint/config-conventional'],
  rules: {
    'header-max-length': [2, "always", 120],
    'body-max-line-length': [1, "always", 100],
    'type-enum': [2, "always", ["breaking", "feat", "fix", "refactor", "config", "test", "docs", "build","chore", "ci", "perf", "no-release"]]
  }
};

module.exports = Configuration;