const commitAnalyzerOptions = {
    preset: 'angular',
    releaseRules: [
        { type: 'build', release: 'patch' },
        { type: 'ci', release: false },
        { type: 'docs', release: false },
        { type: 'refactor', release: 'patch' },
        { type: 'test', release: false },
        { type: 'config', release: 'patch' },
        { type: 'breaking', release: 'major' },
        { scope: 'no-release', release: false },
        { scope: 'test', release: false },
    ],
    parserOpts: {
        noteKeywords: [],
    },
};
const releaseNotesGeneratorOptions = {
    writerOpts: {
        transform: (commit, context) => {
            const issues = [];

            if (commit.type === 'breaking') {
                commit.type = 'Breaking';
            } else if (commit.type === 'feat') {
                commit.type = 'Features';
            } else if (commit.type === 'fix') {
                commit.type = 'Bug Fixes';
            } else if (commit.type === 'refactor') {
                commit.type = 'Code Refactoring';
            } else if (commit.type === 'config') {
                commit.type = 'Config';
            } else if (commit.type === 'test') {
                commit.type = 'Tests';
            } else if (commit.type === 'docs') {
                commit.type = 'Documentation';
            } else if (commit.type === 'build') {
                commit.type = 'Build';
            } else if (commit.type === 'ci') {
                commit.type = 'CI';
            } else if (commit.type === 'perf') {
                commit.type = 'Performance';
            } else if (commit.type === 'no-release') {
                return;
            }
            if (typeof commit.hash === 'string') {
                commit.shortHash = commit.hash.substring(0, 7);
            }

            if (typeof commit.subject === 'string') {
                let url = context.repository
                    ? `${context.host}/${context.owner}/${context.repository}`
                    : context.repoUrl;
                if (url) {
                    url = `${url}/issues/`;
                    // Issue URLs.
                    commit.subject = commit.subject.replace(/#([0-9]+)/g, (_, issue) => {
                        issues.push(issue);
                        return `[#${issue}](${url}${issue})`;
                    });
                }
                if (context.host) {
                    // User URLs.
                    commit.subject = commit.subject.replace(
                        /\B@([a-z0-9](?:-?[a-z0-9/]){0,38})/g,
                        (_, username) => {
                            if (username.includes('/')) {
                                return `@${username}`;
                            }

                            return `[@${username}](${context.host}/${username})`;
                        }
                    );
                }
            }

            // remove references that already appear in the subject
            commit.references = commit.references.filter((reference) => {
                if (issues.indexOf(reference.issue) === -1) {
                    return true;
                }

                return false;
            });

            return commit;
        },
    },
};

module.exports = {
    debug: true,
    branches: [
        "master",
        { name: "release/+([0-9]).+([0-9]).+([0-9])", channel: "next", prerelease: "RC" }
    ],
    plugins: [
        ['@semantic-release/commit-analyzer', commitAnalyzerOptions], // analyze commits with conventional-changelog 
        ['@semantic-release/release-notes-generator', releaseNotesGeneratorOptions], // generate changelog content with conventional-changelog
        '@semantic-release/changelog', // updates the changelog file
        ["@droidsolutions-oss/semantic-release-update-file", {
            "files": [
                {
                    "path": [
                        "Trinity/Trinity.csproj",
                        "Trinity.Virtuoso/Trinity.Virtuoso.csproj"],
                    "type": "xml",
                    "replacements": [
                        { "key": "Version", "value": "${nextRelease.version}" },
                        { "key": "VersionPrefix", "value": "${nextRelease.version}" },
                        { "key": "PackageVersion", "value": "${nextRelease.version}" },
                        { "key": "AssemblyVersion", "value": "${nextRelease.version}" },
                        { "key": "FileVersion", "value": "${nextRelease.version}" },
                        { "key": "InformationalVersion", "value": "${nextRelease.version}" }]
                }
            ]
        }
        ],
        '@semantic-release/gitlab', // creating a git tag
        ['@semantic-release/git', {
            "assets": [
                'CHANGELOG.md',
                'package.json',
                'package-lock.json',
                'npm-shrinkwrap.json',
                "Trinity/Trinity.csproj",
                "Trinity.Virtuoso/Trinity.Virtuoso.csproj"
            ]
        }], // creating a new version commit
        ["@semantic-release/exec", {
            "prepareCmd": "dotnet nuget restore | dotnet build -c Release",
            "publishCmd": "dotnet nuget push \"Build/Release/**/*.nupkg\" --source gitlab"
        }]
    ]
}